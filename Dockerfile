FROM openjdk:8-jre
RUN rm -f /etc/localtime \
&& ln -sv /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
&& echo "Asia/Shanghai" > /etc/timezone
COPY target/*.jar /runhome/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/runhome/app.jar"]

